package exercises.utils;

import java.util.Calendar;
import java.util.Date;
import java.util.logging.Logger;

public class DateUtils {
		
	/**
	 * Return Date with rest (year - numberOfyears)
	 * @param date
	 * @param numberOfyears
	 * @return
	 */
	public static Date calculateYearAgo(Date date, int numberOfyears) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		cal.add(Calendar.YEAR, -numberOfyears);
		return cal.getTime();
	}

	public static Date calculateMinutesAgo(Date date, int numberOfMinutes) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		cal.add(Calendar.MINUTE, -numberOfMinutes);
		return cal.getTime();
	}

	public static long calculateDays(Date beginDate, Date endDate) {
		long milisec = endDate.getTime()-beginDate.getTime();				
		long days = milisec/1000/60/60/24;
		return days;
	}
	
	/**
	 * Verify meetings cannot last less than 15 minutes and more than 3 hours
	 * 
	 * @param beginDateHourMin
	 * @param endDateHourMin
	 * @return
	 */
	public static boolean isRangeValid(Date beginDateHourMin, Date endDateHourMin) {
		System.out.println("	verifying ... [beginDateHourMin="+beginDateHourMin+", endDateHourMin="+endDateHourMin+"]");
		boolean result = false;
		if (beginDateHourMin!=null && endDateHourMin!=null) {
			long milisec = endDateHourMin.getTime()-beginDateHourMin.getTime();	
			//I calculate the minutes
			long minutes = milisec/1000/60;
			System.out.println("	diference minutes : "+minutes);
			if (minutes >= 15 && minutes <= 180 ) {
				result = true;
			}
			System.out.println("	result : "+result);
		}
		return result;
	}
	/**
	 * Change the minutes to the actual hour
	 * @param minutes
	 * @return
	 */
	public static Calendar setMinutes( int minutes ) {
		Calendar cToday = Calendar.getInstance();
		cToday.set(Calendar.MINUTE, ( Calendar.MINUTE + minutes) );
		return cToday;
	}

}
