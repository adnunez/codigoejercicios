package exercises.two.dao;

import java.util.Date;

import exercises.two.dao.interfaces.IReservationDao;
import exercises.two.model.Room;
import exercises.utils.DateUtils;

public class ReservationDao implements IReservationDao{

	public Boolean isAvailableRoom(Room room, Date beginDateHourMin, Date endDateHourMin) {
		Boolean isAvailable = Boolean.FALSE;
		
		if (DateUtils.isRangeValid(beginDateHourMin, endDateHourMin)) {
			StringBuilder sbQuery = new StringBuilder(
					"EXIST SELECT 1"
							+ "FROM Reservation r"
							+ "JOIN Room ro on (r.room_id = ro.id)"
							+ "WHERE ro.id="+room.getId()
							+ "AND (   r.begin_date BETWEEN ("+beginDateHourMin+" AND "+endDateHourMin+")"
							+ "		OR r.end_date BETWEEN ("+beginDateHourMin+" AND "+endDateHourMin+")"
							+ ")"
					);
			String query = sbQuery.toString();
			
			isAvailable = getSession().createSQLQuery(query).list();
		}
		
		return isAvailable;
	}
}
