package exercises.two.dao.interfaces;

import java.util.Date;

import exercises.two.model.Room;

public interface IReservationDao {
	public Boolean isAvailableRoom(Room room,  Date beginDateHourMin, Date endDateHourMin);
}
