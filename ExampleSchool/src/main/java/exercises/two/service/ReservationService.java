package exercises.two.service;


import java.util.Date;

import exercises.two.dao.ReservationDao;
import exercises.two.model.Room;
import exercises.two.service.interfaces.IReservationService;

@Service(value = "reservationService")
public class ReservationService implements IReservationService{

	@Inject
	private ReservationDao reservationDao;
	
	public Boolean isAvailableRoom(Room room, Date beginDateHourMin, Date endDateHourMin) {
		return reservationDao.isAvailableRoom(room, beginDateHourMin, endDateHourMin);
	}
	
}