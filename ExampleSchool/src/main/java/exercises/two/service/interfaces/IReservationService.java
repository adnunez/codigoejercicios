package exercises.two.service.interfaces;

import java.util.Date;

import exercises.two.model.Room;

public interface IReservationService {

	public Boolean isAvailableRoom(Room room, Date beginDateHourMin, Date endDateHourMin);
}
