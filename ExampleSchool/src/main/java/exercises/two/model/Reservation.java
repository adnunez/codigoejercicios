package exercises.two.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import exercises.model.interfaces.IModelEntity;

@Entity
@Table(name="RESERVATION")
@Inheritance(strategy=InheritanceType.SINGLE_TABLE)
public class Reservation implements IModelEntity {

	/**
	 */
	private static final long serialVersionUID = 7724184760606908140L;

	@Id
    @Column(name="id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

	@ManyToOne()
	@JoinColumn(name="room_id")
	private Room room;

    @Column(name="begin_date_hour")
	private Date beginDateHour;
    
    @Column(name="end_date_hour")
	private Date endDateHour;
    
	@ManyToOne()
	@JoinColumn(name="customer_id")
	private Customer customer;
	
	public Reservation() {
		// TODO Auto-generated constructor stub
	}
	
	public Reservation(Room room, Date beginDateHour, Date endDateHour, Customer customer) {
		this.room = room;
		this.beginDateHour = beginDateHour;
		this.endDateHour = endDateHour;
		this.customer = customer;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Room getRoom() {
		return room;
	}

	public void setRoom(Room room) {
		this.room = room;
	}

	public Date getBeginDateHour() {
		return beginDateHour;
	}

	public void setBeginDateHour(Date beginDateHour) {
		this.beginDateHour = beginDateHour;
	}

	public Date getEndDateHour() {
		return endDateHour;
	}

	public void setEndDateHour(Date endDateHour) {
		this.endDateHour = endDateHour;
	}

	public Customer getCustomer() {
		return customer;
	}

	public void setCustomer(Customer customer) {
		this.customer = customer;
	}

}
