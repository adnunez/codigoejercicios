package exercises.two.model;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import exercises.model.interfaces.IModelEntity;

@Entity
@Table(name="CUSTOMER")
@Inheritance(strategy=InheritanceType.SINGLE_TABLE)
public class Customer implements IModelEntity {

	/**
	 */
	private static final long serialVersionUID = -6053585319031162512L;

	@Id
    @Column(name="id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
	
    @Column(name="name")	
	private String name;
    
    @Column(name="address")	
	private String address;
    
    @Column(name="phone")	
	private String phone;
    
    @Column(name="company")	
	private String company;

	@OneToMany(mappedBy="customer", cascade = CascadeType.ALL)
	private List<Reservation> reservations = new ArrayList<Reservation>();
	
	public Customer() {
		// TODO Auto-generated constructor stub
	}
	
	public Customer(String name, String address, String phone, String company) {
		this.name = name;
		this.address = address;
		this.phone = phone;
		this.company = company;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getCompany() {
		return company;
	}

	public void setCompany(String company) {
		this.company = company;
	}
	
	public List<Reservation> getReservations() {
		return reservations;
	}
	
	public void setReservations(List<Reservation> reservations) {
		this.reservations = reservations;
	}
}
