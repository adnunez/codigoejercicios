package exercises.two.model;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import exercises.model.interfaces.IModelEntity;

@Entity
@Table(name="ROOM")
@Inheritance(strategy=InheritanceType.SINGLE_TABLE)
public class Room implements IModelEntity {

    /**
	 * 
	 */
	private static final long serialVersionUID = -5484748048625197216L;
	
	@Id
    @Column(name="id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
	
	@Column(name="name")
	private String name;
	
	@Column(name="capacity")
	private Integer capacity;
	
	@Column(name="location")
	private String location;
	
	@OneToMany(mappedBy="room", cascade = CascadeType.ALL)
	private List<Reservation> reservations = new ArrayList<Reservation>();

	public Room() {
		// TODO Auto-generated constructor stub
	}
	public Room(String name, Integer capacity, String location) {
		this.name = name;
		this.capacity = capacity;
		this.location = location;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Integer getCapacity() {
		return capacity;
	}

	public void setCapacity(Integer capacity) {
		this.capacity = capacity;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}
	
	public List<Reservation> getReservations() {
		return reservations;
	}
	
	public void setReservations(List<Reservation> reservations) {
		this.reservations = reservations;
	}

}
