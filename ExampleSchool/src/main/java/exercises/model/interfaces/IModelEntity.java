package exercises.model.interfaces;

import java.io.Serializable;

public interface IModelEntity extends Serializable{
	public Long getId();
	public void setId(Long id);
}
