package exercises.one.school.service;

import java.util.Date;
import java.util.List;

import exercises.one.school.dao.SchoolDao;
import exercises.one.school.model.entities.Student;
import exercises.one.school.model.entities.Subject;
import exercises.one.school.service.interfaces.ISchoolService;

@Service(value = "schoolService")
public class SchoolService implements ISchoolService{

	@Inject
	private SchoolDao schoolDao;
	
	public List<Student> getStudentGroupByLastName(String letter) {
		return schoolDao.getStudentGroupByLastName(letter);
	}

	public List<Student> getStudentsForSubject(Subject subject){
		return schoolDao.getStudentsForSubject(subject);
	}

	public List<Student> getStudentsBetweenYears(Date fromDate, Date toDate){
		return schoolDao.getStudentsBetweenYears(fromDate, toDate);
	}
	
	public SchoolDao getSchoolDao() {
		return schoolDao;
	}
	
	public void setSchoolDao(SchoolDao schoolDao) {
		this.schoolDao = schoolDao;
	}
}
