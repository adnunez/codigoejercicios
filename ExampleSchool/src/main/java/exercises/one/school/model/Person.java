package exercises.one.school.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import exercises.one.school.model.interfaces.IPerson;

/**
 * Entity with properties for Person
 * @author dany
 *
 */
public abstract class Person implements IPerson {
	
    /**
	 * 
	 */
	private static final long serialVersionUID = -24982814667261662L;

	@Id
    @Column(name="ID")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    
	@Column(name="first_name")
	private String firstName;
	
	@Column(name="last_name")
	private String lastName;
	
	@Column(name="birth_date")
	private Date birthDate;

	public Person() {
		// TODO Auto-generated constructor stub
	}

	public Person(String firstName, String lastName, Date birthDate) {
		super();
		this.firstName = firstName;
		this.lastName = lastName;
		this.birthDate = birthDate;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public Date getBirthDate() {
		return birthDate;
	}

	public void setBirthDate(Date birthDate) {
		this.birthDate = birthDate;
	}
	
	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder("firstName:"+firstName
				+ ", lastName:"+lastName
				+ ", birthDate:"+birthDate
				);
		return sb.toString();
	}
}
