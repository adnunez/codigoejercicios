package exercises.one.school.model.interfaces;

/**
 * Define methods for a Student
 * 
 * @author dany
 *
 */
public interface IStudent extends IPerson {
	Integer getFileNumberStudent();
	void setFileNumberStudent(Integer fileNumberStudent);
	Float getAverage();
	void setAverage(Float average);
}
