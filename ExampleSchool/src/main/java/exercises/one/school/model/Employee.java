package exercises.one.school.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.DiscriminatorColumn;
import javax.persistence.DiscriminatorType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.Table;

import exercises.one.school.model.interfaces.IEmployee;

/**
 * Define Abstract class and create a table Employee with a discriminator ROLE
 * ROLE : may be a PRINCIPAL, JANITOR or TEACHER
 * 
 * @author dany
 *
 */

@Entity
@Table(name="EMPLOYEE")
@Inheritance(strategy=InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(name="ROLE", discriminatorType=DiscriminatorType.STRING)
public abstract class Employee extends Person implements IEmployee {

    /**
	 */
	private static final long serialVersionUID = 2003636439664832965L;

	
	@Column(name="file_number_employee")
	private Integer fileNumberEmployee;


	public Integer getFileNumberEmployee() {
		return fileNumberEmployee;
	}

	public void setFileNumberEmployee(Integer fileNumberEmployee) {
		this.fileNumberEmployee = fileNumberEmployee;
	}

	
	protected abstract Float calculateSalary();
	
	@Override
	public String toString() {
		String toString = super.toString();
		StringBuilder sb = new StringBuilder(toString
				+ ", number Employee:"+fileNumberEmployee
				);
		return sb.toString();
	}
}
