package exercises.one.school.model.entities;

import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

import exercises.model.interfaces.IModelEntity;
import exercises.one.school.model.Employee;

@Entity
@Table(name="SUBJECT")
public class Subject  implements IModelEntity{

    @Id
    @Column(name="ID")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    
	@Column(name="name")
	private String name;

	@Column(name="year")
	private String year;
	
	@ManyToMany(mappedBy = "subject")
    private Set<Student> students;

	@ManyToMany(mappedBy = "employee")
    private Set<Employee> teachers;
	/**
	 * 
	 */
	private static final long serialVersionUID = 1207808615896687477L;
	
	public Subject() {
		// TODO Auto-generated constructor stub
	}
	
	public Subject(String name, String year) {
		this.name = name;
		this.year = year;
	}
	
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getYear() {
		return year;
	}

	public void setYear(String year) {
		this.year = year;
	}

	public Set<Student> getStudents() {
		return students;
	}

	public void setStudents(Set<Student> students) {
		this.students = students;
	}

	public Set<Employee> getTeachers() {
		return teachers;
	}

	public void setTeachers(Set<Employee> teachers) {
		this.teachers = teachers;
	}
	
	@Override
	public String toString() {
		
		StringBuilder sb = new StringBuilder("Subject ["
				+ " name:"+name+", year:"+year
				+ "]");
		
		return sb.toString();
	}
	
}
