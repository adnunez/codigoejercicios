package exercises.one.school.model.interfaces;

/**
 * Define methods for interface Employee 
 * 
 * @author dany
 */
public interface IEmployee extends IPerson{
	
	public Integer getFileNumberEmployee();
	public void setFileNumberEmployee(Integer fileNumberEmployee);

}
