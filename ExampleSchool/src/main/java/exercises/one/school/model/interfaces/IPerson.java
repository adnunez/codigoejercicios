package exercises.one.school.model.interfaces;

import java.util.Date;

import exercises.model.interfaces.IModelEntity;

/**
 * Define methods for interface Person
 * 
 * @author dany
 */
public interface IPerson extends IModelEntity{

	 String getFirstName();
	 void setFirstName(String firstName);
	 String getLastName();
	 void setLastName(String lastName);
	 Date getBirthDate();
	 void setBirthDate(Date birthDate);
}
