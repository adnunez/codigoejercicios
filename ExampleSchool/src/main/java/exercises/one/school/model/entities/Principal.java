package exercises.one.school.model.entities;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

import exercises.one.school.model.Employee;

@Entity
@DiscriminatorValue(value="PRINCIPAL")
public class Principal extends Employee {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2892606910752075056L;

	@Column(name="name_school")
	private String nameSchool;
	
	public Principal() {
		// TODO Auto-generated constructor stub
	}

	public Principal(String firstName, String lastName, Date birthDate, Integer fileNumberEmployee, String nameSchool) {
		setFirstName(firstName);
		setLastName(lastName);
		setBirthDate(birthDate);
		setFileNumberEmployee(fileNumberEmployee);
		this.nameSchool = nameSchool;
	}

	
	public String getNameSchool() {
		return nameSchool;
	}
	
	public void setNameSchool(String nameSchool) {
		this.nameSchool = nameSchool;
	}

	@Override
	protected Float calculateSalary() {
		// TODO Auto-generated method stub
		return null;
	}
	
	@Override
	public String toString() {
		String toStringParent = super.toString();
		StringBuilder sb = new StringBuilder("Principal ["
				+ toStringParent + ", school:"+nameSchool
				+ "]");
		
		return sb.toString();
	}
}
