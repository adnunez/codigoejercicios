package exercises.one.school.model.entities;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

import exercises.one.school.model.Person;
import exercises.one.school.model.interfaces.IStudent;

@Entity
@Table(name="STUDENT")
public class Student extends Person implements IStudent{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 4003494907084960480L;

	@Column(name="average")
	private Float average;
	
	@Column(name="file_number_student")
	private Integer fileNumberStudent;
	
	public String getFirtCaracterLastName() {
		return getLastName()!=null ? String.valueOf(  getLastName().charAt(0)  ) : "";
	}
	
	@ManyToMany(cascade = {CascadeType.ALL})
	@JoinTable(
			name="student_subject", 
			joinColumns={@JoinColumn(name="student_id")}, 
			inverseJoinColumns={@JoinColumn(name="subject_id")}
	)
	private Set<Subject> subjects=new HashSet<Subject>();
	
	public Student() {
		// TODO Auto-generated constructor stub
	}
	public Student(String firstName, String lastName, Date birthDate, Integer fileNumberStudent, Float average) {
		super(firstName, lastName, birthDate);
		this.fileNumberStudent = fileNumberStudent;
		this.average = average;
	}
	
	public Float getAverage() {
		return average;
	}
	
	public void setAverage(Float average) {
		this.average = average;
	}

	public Integer getFileNumberStudent() {
		return fileNumberStudent;
	}
	
	public void setFileNumberStudent(Integer fileNumberStudent) {
		this.fileNumberStudent = fileNumberStudent;
	}
	
	public void addSubject(Subject newSubject) {
		subjects.add(newSubject);
	}
	
	public Set<Subject> getSubjects() {
		return subjects;
	}
	
	@Override
	public String toString() {
		
		StringBuilder namesSubject= new StringBuilder();
		for (Subject subject : subjects) {
			namesSubject.append(subject.getName()).append(",");
		}
		namesSubject.delete(namesSubject.length()-1, namesSubject.length());
		
		String toStringParent = super.toString();
		StringBuilder sb = new StringBuilder("Student ["
				+ toStringParent
				+ ", average:"+average
				+ ", number Student:"+fileNumberStudent
				+ ", names Subject:"+namesSubject
				+"]"
				);
		return sb.toString();
	}
}
