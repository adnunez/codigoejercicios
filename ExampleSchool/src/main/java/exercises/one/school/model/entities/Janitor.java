package exercises.one.school.model.entities;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

import exercises.one.school.model.Employee;

@Entity
@DiscriminatorValue(value="JANITOR")
public class Janitor extends Employee {

	private static final long serialVersionUID = 8132409818219071132L;

	@Column(name="turn")
	private String turn; //morning or afternoon

	public Janitor() {
	}
	
	public Janitor(String firstName, String lastName, Date birthDate, Integer fileNumberEmployee, String turn) {
		setFirstName(firstName);
		setLastName(lastName);
		setBirthDate(birthDate);
		setFileNumberEmployee(fileNumberEmployee);
		this.turn = turn;
	}

	public String getTurn() {
		return turn;
	}

	public void setTurn(String turn) {
		this.turn = turn;
	}

	@Override
	protected Float calculateSalary() {
		// TODO Auto-generated method stub
		return null;
	}
	
	
	@Override
	public String toString() {
		String toStringParent = super.toString();
		StringBuilder sb = new StringBuilder("Janitor ["
				+ toStringParent + ", turn:"+turn
				+ "]");
		
		return sb.toString();
	}
}
