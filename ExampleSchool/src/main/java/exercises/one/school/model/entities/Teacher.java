package exercises.one.school.model.entities;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;

import exercises.one.school.model.Employee;

/**
 * Teacher is a Employee and his relations is with Subjects.
 * 
 * @author dany
 *
 */

@Entity
@DiscriminatorValue(value="TEACHER")
public class Teacher extends Employee {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2668444961859740226L;
	
	@ManyToMany(cascade = {CascadeType.ALL})
	@JoinTable(
			name="teacher_subject", 
			joinColumns={@JoinColumn(name="teacher_id")}, 
			inverseJoinColumns={@JoinColumn(name="subject_id")}
	)
	private Set<Subject> subjects=new HashSet<Subject>();
	
	public Teacher() {
	}
	
	public Teacher(String firstName, String lastName, Date birthDate, Integer fileNumberEmployee) {
		setFirstName(firstName);
		setLastName(lastName);
		setBirthDate(birthDate);
		setFileNumberEmployee(fileNumberEmployee);
	}

	
	public Set<Subject> getSubjects() {
		return subjects;
	}
	public void setSubjects(Set<Subject> subjects) {
		this.subjects = subjects;
	}

	@Override
	protected Float calculateSalary() {
		// TODO Auto-generated method stub
		return null;
	}
	
	public void addSubject(Subject newSubject) {
		subjects.add(newSubject);
	}
	
	@Override
	public String toString() {
		String toStringParent = super.toString();
		StringBuilder namesSubject= new StringBuilder();
		for (Subject subject : subjects) {
			namesSubject.append(subject.getName()).append(",");
		}
		namesSubject.delete(namesSubject.length()-1, namesSubject.length());
		
		StringBuilder sb = new StringBuilder("Teacher ["
				+ toStringParent + ", subjects:"+namesSubject.toString()
				+ "]");
		
		return sb.toString();
	}
}
