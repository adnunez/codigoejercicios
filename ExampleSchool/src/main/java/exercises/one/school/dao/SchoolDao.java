package exercises.one.school.dao;

import java.util.Date;
import java.util.List;

import exercises.one.school.dao.interfaces.ISchoolDao;
import exercises.one.school.model.entities.Student;
import exercises.one.school.model.entities.Subject;
import exercises.utils.DateUtils;

/**
 * Define all querys methods for retrieve info at School Entities
 * 
 * @author dany
 *
 */
public class SchoolDao implements ISchoolDao{
	
	
	//...
	/**
	 * Return List of all the students in the school grouped by the first letter 
	 * of their last name.
	 * 
	 * @return List<Student>
	 */
	public List<Student> getStudentGroupByLastName(String letter){
		//Assuming we have a Role table with the following values
		
		//Role
		//-------------------
		//  ID,	DESCRIPTION
		//-------------------
		// 'P', 'PRINCIPAL'
		// 'J', 'JANITOR'
		// 'S', 'STUDENT'

		//MODEL 1
		//For this model for looking Student I need Join with a Role
		String role_description ="STUDENT";
		StringBuilder queryModel1 = new StringBuilder("SELECT p.*"
				+ "FROM Person p"
				+ "JOIN Role r on (p.role_id = r.id)"
				+ "WHERE r.description="+role_description
				+ "AND p.last_name LIKE "+letter+"%"
				+ "ORDER BY p.last_name"
				);

		//MODEL 2
		//For this model the entity Student is separated and this query is more simple that Model 1
		StringBuilder queryModel2 = new StringBuilder("SELECT st.*"
				+ "FROM Student st"
				+ "AND st.last_name LIKE "+letter+"%"
				+ "ORDER BY st.last_name"
				);
		String query = queryModel1.toString();
		
		List<Student> students = getSession().createSQLQuery(query).list();
		return students;
	}
	
	public List<Student> getStudentsForSubject(Subject subject){
		//Assuming we have a Role table with the following values
		
		//Role
		//-------------------
		//  ID,	DESCRIPTION
		//-------------------
		// 'P', 'PRINCIPAL'
		// 'J', 'JANITOR'
		// 'S', 'STUDENT'
		
		//option for Model 1
		//For this model for looking Student I need 3 Join
		String role_description ="STUDENT";
		StringBuilder queryModel1 = new StringBuilder(
				"SELECT distinct p.* "
				+ "FROM person p "
				+ "JOIN role r on (p.role_id = r.id) "
				+ "JOIN person_subject ps on (p.id = ps.person_id) "
				+ "JOIN subject s on (s.id = ps.subject_id)"
				+ "WHERE s.name = "+subject.getName()
				+ "AND r.description="+role_description);

		//option for Model 2
		//For this model for looking Student I need 2 Join, is more simple and quickly
		StringBuilder queryModel2 = new StringBuilder(
				"SELECT distinct st.* "
				+ "FROM student st"
				+ "JOIN subject_student ss on (st.id = ss.student_id) "
				+ "JOIN subject s on (s.id = ss.subject_id)"
				+ "WHERE s.name = "+subject.getName()
				);

		String query = queryModel1.toString();
		List<Student> students = getSession().createSQLQuery(query).list();
		return students;
	}
	

	/**
	 * returns all the students with age between from and to
	 * @param from
	 * @param to
	 * @return
	 */
	public List<Student> getStudentsBetweenYears(Date from, Date to){
		
		Date dateTo = DateUtils.calculateYearAgo(new Date(), 19);
		
		//MODEL 1
		//For this model for looking Student I need Join with a Role
		String role_description ="STUDENT";
		StringBuilder queryModel1 = new StringBuilder(
				"SELECT p.* "
				+ "FROM person p "
				+ "JOIN role r on (p.role_id = r.id) "
				+ "WHERE p.BIRTH_DATE >"+from+" AND p.BIRTH_DATE <"+to
				+ "AND r.description="+role_description
				);
		
		//MODEL 2
		//For this model the entity Student is separated and this query is 
		//faster and more efficient that Model 1
		StringBuilder queryModel2 = new StringBuilder(
				"SELECT t.* "
				+ "FROM student t "
				+ "WHERE t.BIRTH_DATE >"+from+" AND t.BIRTH_DATE <"+to
				);
		
		String query = queryModel1.toString();
		List<Student> students = getSession().createSQLQuery(query).list();
		return students;
		
	}

}
