package exercises.one.school.dao.interfaces;

import java.util.Date;
import java.util.List;

import exercises.one.school.model.entities.Student;
import exercises.one.school.model.entities.Subject;

public interface ISchoolDao {
	List<Student> getStudentGroupByLastName(String letter);
	List<Student> getStudentsForSubject(Subject subject);
	List<Student> getStudentsBetweenYears(Date from, Date to);
}
