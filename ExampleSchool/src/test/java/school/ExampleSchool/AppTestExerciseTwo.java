package school.ExampleSchool;

import java.util.Calendar;
import java.util.Date;

import exercises.two.model.Customer;
import exercises.two.model.Reservation;
import exercises.two.model.Room;
import exercises.utils.DateUtils;
import junit.framework.TestCase;

/**
 * Unit test for Verify Rules Times for Reservations Rooms.
 */
public class AppTestExerciseTwo extends TestCase
{
	Room room;
	Reservation reservation;
	Customer customer;

	public static boolean verifyMinutes(int minutesBegin, int minutesEnd) {
		System.out.println("minutesBegin:"+minutesBegin+", minutesEnd:"+minutesEnd);
		Calendar cTodayBegin = DateUtils.setMinutes(minutesBegin);
		Calendar cTodayEnd = DateUtils.setMinutes(minutesEnd);
		return DateUtils.isRangeValid(cTodayBegin.getTime(), cTodayEnd.getTime());
	}
	
	/**
	 * Minor a 15 minutes
	 * I verify actual time < 5 minutes and actual time > 5 minutes
	 */
	public void ttestVerifyMinorFifteenMinutes() {
		assertEquals(false, verifyMinutes(-5, 5));
	}

	/**
	 * Major or equals a 15 minutes
	 */
	public void ttestVerifyMajorFifteenMinutes() {
		assertEquals(true, verifyMinutes(0, 16));
	}
	
	/**
	 * Minor a 180 minutes
	 */
	public void ttestVerifyMinorOneHundredEightyMinutes() {
		assertEquals(true, verifyMinutes(0, 170));
	}

	/**
	 * Major a 180 minutes
	 */
	public void ttestVerifyMajorOneHundredEightyMinutes() {
		assertEquals(false, verifyMinutes(0, 190));
	}
	

	public void ttestCreateRoom() {
		room = new Room("Room One", 10, "Address One");
	}
	
	public void ttestCreateCustomer() {
		customer = new Customer("John", "Av.Address", "01-12345678", "Company");
	}
	

	public void testCreateReservation_OutRange_case_One() {
		//-------------------------------------------------------
		//Verify that this time (10 minutes) is not valid range ( Valid[ 15 - 180 ] minutes ) 
		//-------------------------------------------------------
		boolean isRangeValid = verifyMinutes(0, 10);
		assertEquals(false, isRangeValid);
	}

	public void testCreateReservation_OutRange_case_Two() {
		//-------------------------------------------------------
		//Verify that this time (240 minutes) is not valid range ( Valid[ 15 - 180 ] minutes ) 
		//-------------------------------------------------------		cTodayBegin = Calendar.getInstance();
		boolean isRangeValid = verifyMinutes(0, 240);
		assertEquals(false, isRangeValid);

		
		//reservation = new Reservation(room, beginDateHour, endDateHour, customer)
		
	}

	public void testCreateReservation_range_valid() {
		//-------------------------------------------------------
		//Verify that this time (20 minutes) is valid range ( Valid[ 15 - 180 ] minutes ) 
		//-------------------------------------------------------
		boolean isRangeValid = verifyMinutes(0, 20);
		assertEquals(true, isRangeValid);
	}

	public void testCreateReservationOk() {
		room = new Room("Room One", 10, "Address One");
		Calendar beginDateHour = Calendar.getInstance();
		Calendar endDateHour = Calendar.getInstance();
		endDateHour.set(Calendar.MINUTE, ( Calendar.MINUTE + 20) );
		reservation = new Reservation(room, beginDateHour.getTime(), endDateHour.getTime(), customer);
		assertEquals("Room One", reservation.getRoom().getName());
	}

}
