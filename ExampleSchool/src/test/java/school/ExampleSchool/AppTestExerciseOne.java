package school.ExampleSchool;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import org.apache.log4j.Logger;

import exercises.one.school.model.entities.Janitor;
import exercises.one.school.model.entities.Principal;
import exercises.one.school.model.entities.Student;
import exercises.one.school.model.entities.Subject;
import exercises.one.school.model.entities.Teacher;
import exercises.utils.DateUtils;
import junit.framework.TestCase;

public class AppTestExerciseOne extends TestCase{
	
	
	Date today = Calendar.getInstance().getTime();

	Subject s1= new Subject("History", "1");
	Subject s2= new Subject("Economics", "1");
	Subject s3= new Subject("Maths", "1");
	
	Student student1 = new Student("Antony","Smith",DateUtils.calculateYearAgo(today, 14),100, 0f);
	Student student2 = new Student("Emma","Anderson",DateUtils.calculateYearAgo(today, 15),101, 0f);
	Student student3 = new Student("Jonny","Smith",DateUtils.calculateYearAgo(today, 16),102, 0f);
	Student student4 = new Student("Kate","Thompson",DateUtils.calculateYearAgo(today, 17),103, 0f);
	Student student5 = new Student("Ralph","DiMarco",DateUtils.calculateYearAgo(today, 14),100, 0f);
	Student student6 = new Student("Michael","Simpson",DateUtils.calculateYearAgo(today, 14),100, 0f);
	Student student7 = new Student("Tom","Sunny",DateUtils.calculateYearAgo(today, 14),100, 0f);
	Student student8 = new Student("Andy","Sullivan",DateUtils.calculateYearAgo(today, 14),100, 0f);

	
	public void testCreatePrincipal() {
		Date today = Calendar.getInstance().getTime();
		Principal principal = new Principal("John", "Doe", DateUtils.calculateYearAgo(today, 49), 1, "School N1");
		
		assertEquals("School N1", principal.getNameSchool());
	}
	
	public void testCreateJanitors() {
		Janitor j1 = new Janitor("Mike", "Harry", DateUtils.calculateYearAgo(today, 35), 2, "morning"); 
		Janitor j2 = new Janitor("Rebecca", "Aniston", DateUtils.calculateYearAgo(today, 34), 3, "aftenoon");

		assertEquals("morning", j1.getTurn());
		assertEquals("aftenoon", j2.getTurn());
	}

	public void testCreateTeachers() {
		Teacher t1 = new Teacher("Jeniffer", "Smith", DateUtils.calculateYearAgo(today, 28), 10);
		Teacher t2 = new Teacher("Daniel", "Neeson", DateUtils.calculateYearAgo(today, 26), 11);
		Teacher t3 = new Teacher("Kate", "Andrews", DateUtils.calculateYearAgo(today, 27), 12);
		t1.addSubject(s1);
		t2.addSubject(s2);
		t3.addSubject(s3);

		assertEquals( Integer.valueOf(10), t1.getFileNumberEmployee());
		assertEquals( "Daniel", t2.getFirstName());
		assertEquals( "Andrews", t3.getLastName());
	}
	
	public void testCreateSubjects() {
		this.s1= new Subject("History", "1 year");
		this.s2= new Subject("Economics", "2 year");
		this.s3= new Subject("Maths", "1 yea");

		assertEquals( "1 year", s1.getYear());
		assertEquals( true, ( s2.getStudents()==null || s2.getStudents().isEmpty()));
		assertEquals( "Maths", s3.getName());
	}
	
	public void testAssociateStudentsSubjects() {

		student1.addSubject(s1);

		student2.addSubject(s1);
		student2.addSubject(s2);
		
		student3.addSubject(s1);
		student3.addSubject(s3);

		student4.addSubject(s1);
		student4.addSubject(s2);
		student4.addSubject(s3);	
		
		assertEquals("Antony", student1.getFirstName());
		assertEquals("Anderson", student2.getLastName());
		assertEquals(Integer.valueOf(102), student3.getFileNumberStudent());
		assertEquals(0f, student4.getAverage());
	}

	public void testStudentsGroupedByFirstLetter() {
		List<Student> students = new ArrayList<Student>();
		students.add(student1);
		students.add(student2);
		students.add(student3);
		students.add(student4);
		students.add(student5);
		students.add(student6);
		students.add(student7);
		students.add(student8);

		Map<String, List<Student>>  map = getStudentGroupByLastName(students);
		for (String key : map.keySet()) {
			System.out.println("key "+key);
			for (Student st : map.get(key)) {
				System.out.println("Student [lastname, firstname] : ["+st.getLastName()+", "+st.getFirstName()+"]");
			}
		}
	}

	/**
	 * Return a Map with a 
	 * 		Key= First Letter the Lastname 
	 * 		Value= List of Students where beginning first letter of Lastname begin with the key 
	 * @param students
	 * @return
	 */
	private static Map<String, List<Student>> getStudentGroupByLastName(List<Student> students) {
		Map<String, List<Student>>  result = new HashMap<String,List<Student>>();
		
		result =  students.stream().collect(  Collectors.groupingBy( 
				(Student::getFirtCaracterLastName)));
		
		return result;
    }
	
	/**
	 * Exercise  C) 
	 * Implement a method that returns all students taking a subject. 
	 * Please consider that the students cannot be repeated, this needs to be verified.
	 */
	public void testStudentsTakeSubject() {
		Subject newSubject = new Subject("History", "1 year");
		
		student1.addSubject(newSubject);
		student2.addSubject(newSubject);
		student3.addSubject(newSubject);
		
		List<Student> students = new ArrayList<Student>();
		students.add(student1);
		students.add(student2);
		students.add(student3);
		students.add(student4);
		students.add(student5);
		
		Set<Student> filterStudents = getFilterStudentBySubject(students, newSubject);
		for (Student st : filterStudents) {
			System.out.println("Student with have the subject["+newSubject.getName()+"] - ["+st.getLastName()+", "+st.getFirstName()+"]");
		}
	}
	
	/**
	 * Get Students that take the subject  
	 * @param students
	 * @param subject
	 * @return Set<Student> 
	 */
    private static Set<Student> getFilterStudentBySubject(List<Student> students, Subject subject) {
    	Set<Student> filterStudents = new HashSet<Student>();
    	if (subject!=null) {
    		//This is one way 
    		for (Student student : students) {
    			if (  student.getSubjects().contains(subject) ) { 
    				filterStudents.add(student);
    			}
    		}
    		//This is other way by example using Java 8 with stream 
    		filterStudents = students.stream()                
    				.filter(  stu ->  stu.getSubjects().contains(subject)  )     
    				.collect(Collectors.toSet());
    	}
        return filterStudents;
    }
	
}
